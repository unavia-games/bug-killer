# Bug Killer

The age-old struggle between developers and bugs continues in this simple co-op adventure!  The endless (and almost meaningless) repetition seeks to drive home the point that software development is a constant struggle against bugs...

> **Note:** This project was entirely inspired by Brackey's - Party Killer, and developed to match simply from playing the game myself and following his devlogs. Seriously, go watch his videos!

This was my first attempt at a co-op game (and actually at finishing something I started) and I would welcome any feedback!

