﻿using Sirenix.OdinInspector;
using System;
using Unavia.Utilities;
using UnityEditor;
using UnityEngine;

public class WallGenerator : MonoBehaviour
{
    #region Fields
    [SerializeField]
    [OnValueChanged("OnSizeChange")]
    private Vector2Int size = new Vector2Int(50, 25);
    [SerializeField]
    [OnValueChanged("OnCutoutSizeChange")]
    private Vector2Int cutoutSize = new Vector2Int(3, 3);

    [SerializeField]
    private GameObject wallPrefab = default;

#if UNITY_EDITOR
    [Button("Generate Walls")]
    private void OnGenerateWalls()
    {
        if (!wallPrefab) return;

        bool shouldReplaceWalls = transform.childCount > 0
            ? EditorUtility.DisplayDialog("Replace Walls?", "Are you sure you want to replace the existing walls?", "Yes", "No")
            : true;
        if (!shouldReplaceWalls) return;

        gameObject.ClearChildren();

        int halfHeight = size.y / 2;
        int halfWidth = size.x / 2;
        for (int y = -halfHeight; y < size.y - halfHeight; y++)
        {
            for (int x = -halfWidth; x < size.x - halfWidth; x++)
            {
                if (Math.Abs(x) < cutoutSize.x && Math.Abs(y) < cutoutSize.y) continue;

                GameObject wall = Instantiate(wallPrefab, new Vector3(x, 0f, y), Quaternion.identity, transform);
                // Disable colliders for performance AND collision detection
                wall.GetComponent<Collider>().enabled = false;
            }
        }
    }
#endif

    private void OnSizeChange()
    {
        size.x = Mathf.Clamp(size.x, 1, 100);
        size.y = Mathf.Clamp(size.y, 1, 100);
    }

    private void OnCutoutSizeChange()
    {
        cutoutSize.x = Mathf.Clamp(cutoutSize.x, 1, size.x / 2 - 2);
        cutoutSize.y = Mathf.Clamp(cutoutSize.y, 1, size.y / 2 - 2);
    }
    #endregion
}
