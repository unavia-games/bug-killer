﻿using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;

public class WallMover : MonoBehaviour
{
    #region Fields
    [ShowInInspector]
    [Range(1f, 20f)]
    private static float smoothness = 5f;
    [ShowInInspector]
    [Range(1f, 5f)]
    private static float perlinMultiplier = 2f;
    [ShowInInspector]
    [Range(0f, 5f)]
    private static float speed = 1f;
    #endregion

    private Transform[] walls;


    #region Unity Methods
    void Start()
    {
        walls = new Transform[transform.childCount];

        for (int i = 0; i < walls.Length; i++)
        {
            walls[i] = transform.GetChild(i);
        }
    }

    void Update()
    {
        walls.ForEach(w =>
        {
            float perlin = Mathf.PerlinNoise(w.position.x / smoothness + Time.time * speed, w.position.z / smoothness + Time.time * speed);
            // NOTE: Walls should always remain at least 1 unit high
            w.localScale = new Vector3(1f, perlin * perlinMultiplier + 1f, 1f);
        });
    }
    #endregion
}
