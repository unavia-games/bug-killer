﻿using UnityEngine;

public class GameLevel : MonoBehaviour
{
    #region Fields
    public string LevelName;
    public Transform BugSpawn = default;
    public Transform ManSpawn = default;
    #endregion
}
