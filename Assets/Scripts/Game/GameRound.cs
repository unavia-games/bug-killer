﻿using System;
using UnityEngine;

[Serializable]
public class GameRound
{
    #region Fields
    public int Number;
    public float Time;
    public PlayerType Winner;
	#endregion

    public GameRound(int number, PlayerType winner, float time)
    {
        Number = number;
        Time = time;
        Winner = winner;
    }
}
