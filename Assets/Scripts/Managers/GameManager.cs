﻿using com.ootii.Messages;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unavia.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : GameSingleton<GameManager>
{
    #region Fields
    // NOTE: Player controllers are created at game start and associated with a parent PlayerInput later!
    [Header("Players")]
    [InfoBox("'Bug' should always be the first player...simply as a joke...")]
    public List<PlayerController> Players;
    [SerializeField]
    private Transform playerParent = default;

    [Header("UI")]
    [SerializeField]
    private MainMenuUI mainMenuUI = default;
    [SerializeField]
    private RoundOverUI roundOverUI = default;
    [SerializeField]
    private GameUI gameUI = default;

    [Header("Game Information")]
    [ReadOnly]
    [SerializeField]
    private int roundNumber;
    /// <summary>
    /// Which level index is the first playable level
    /// </summary>
    [SerializeField]
    [Min(0)]
    private int firstLevelIndex = 1;
    [ReadOnly]
    public List<GameRound> RoundsPlayed;

    [Header("Audio")]
    [SerializeField]
    private AudioClip playerJoinClip = default;
    [SerializeField]
    private AudioClip roundStartAudio = default;
    [SerializeField]
    private AudioClip roundEndAudio = default;

    [Header("Debug")]
    public bool WaitForAllPlayers = true;
    /// <summary>
    /// Which level should always be loaded (keep negative for random)
    /// </summary>
    [SerializeField]
    [Min(-1)]
    [InfoBox("Select a consistent level index (set negative for random)")]
    private int debugLevelIndex = -1;
    [SerializeField]
    [ReadOnly]
    private int currentLevelIndex;
    #endregion

    public int ScoreBug { get { return RoundsPlayed.Count(r => r.Winner == PlayerType.BUG); } }
    public int ScoreMan { get { return RoundsPlayed.Count(r => r.Winner == PlayerType.MAN); } }
    public bool IsRoundInProgress { get; private set; } = false;
    public int RoundNumber { get { return roundNumber; } }
    public bool HaveAllPlayersJoined { get { return Players.Count(x => x.HasJoined) == Players.Count; } }

    private PlayerInputManager playerInputManager;
    // Countdown on "Main Menu" after both players have joined
    private float joinScreenGameDelay = 1f;
    private float roundStartTime;
    // Countdown on "Round Over" screen before showing next level
    private float roundOverCountdown = 2f;


    #region Unity Methods
    private void Awake()
    {
        playerInputManager = GetComponent<PlayerInputManager>();

        RoundsPlayed = new List<GameRound>();
        roundNumber = 1;

        MessageDispatcher.AddListener(GameEventStrings.ROUND__OVER, OnRoundOver);
    }

    private void Start()
    {
        mainMenuUI.Show();

        // Either start with a specified or randomized level
        currentLevelIndex = GetNextLevelIndex(debugLevelIndex);
        SceneManager.LoadSceneAsync(currentLevelIndex, LoadSceneMode.Additive);
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(GameEventStrings.ROUND__OVER, OnRoundOver);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Quit the game
    /// </summary>
    public void Quit()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }

    #region Player Management Methods
    public void OnPlayerJoin(PlayerInput input)
    {
        // Enable joining if max player count has not been reached
        if (playerInputManager.playerCount >= playerInputManager.maxPlayerCount)
        {
            playerInputManager.DisableJoining();
        }

        PlayerController availablePlayer = Players.Find(p => !p.HasJoined);
        if (availablePlayer == null)
        {
            Debug.Log("No player available for new input!");
            // QUESTION: This is probably not the best approach (does it clean up listeners?)...
            Destroy(input.gameObject);
            return;
        }

        // Notify UI that a player has joined
        PlayerJoinData newPlayerData = new PlayerJoinData(availablePlayer.Type);
        MessageDispatcher.SendMessageData(GameEventStrings.PLAYER__JOIN, newPlayerData);

        AudioManager.Instance.PlayEffect(playerJoinClip, 0.4f);

        // Player setup includes moving the "existing" player into the shell spawned by the input manager
        SetupPlayer(availablePlayer, input);

        // Begin the game once all players have joined
        if (HaveAllPlayersJoined || !WaitForAllPlayers)
        {
            StartCoroutine(mainMenuUI.FadeToGame(joinScreenGameDelay, () => {
                RoundsPlayed.Clear();
                roundNumber = 0;

                StartRound();
            }));
        }
    }

    public void OnPlayerLeave(PlayerInput input)
    {
        // Enable joining if max player count has not been reached
        if (playerInputManager.playerCount < playerInputManager.maxPlayerCount)
        {
            playerInputManager.EnableJoining();
        }

        // TODO: Probably should end round until they rejoin...

        PlayerController leavingPlayer = Players.Find(p => p.gameObject.GetComponent<PlayerInput>() == input);
        if (leavingPlayer != null)
        {
            leavingPlayer.RemoveInput(input);
        }
    }

    private void SetupPlayer(PlayerController player, PlayerInput input)
    {
        // Remove the previous shell children, if any (ie. debug model for editor)
        input.gameObject.ClearChildren();

        // Setup the spawned player object (which is just a shell with a PlayerInput component),
        //   before moving the existing player controller into the newly spawned shell.
        //   This allows using different prefabs for players (although requires manual event binding)
        input.transform.SetParent(playerParent);
        player.transform.SetParent(input.gameObject.transform);

        // Players should be disabled before the game starts (prevents movement bugs, etc).
        player.gameObject.SetActive(false);

        // Disable the player's input until game has started
        player.SetInput(input);
        player.DisableInput();
    }

    private void SetupPlayerForRound(PlayerController player, GameLevel levelInfo)
    {
        Transform spawnLocation = player.Type == PlayerType.BUG
            ? levelInfo.BugSpawn
            : levelInfo.ManSpawn;

        // NOTE: Although only the player object is ever moved, both shell and player should be reset (since spawn has changed)
        GameObject playerShell = player.Input.gameObject;
        GameObject playerCharacter = player.gameObject;
        playerShell.transform.SetPositionAndRotation(spawnLocation.position, spawnLocation.rotation);
        playerCharacter.SetActive(true);

        // Reset player transforms, physics, etc
        player.Reset();
    }
    #endregion

    #region Game Management Methods
    private void StartRound()
    {
        roundNumber++;

        // Find the level spawn locations for players
        // NOTE: "Bugs" always get the first spawn location...JOKE!
        GameLevel levelInfo = FindObjectOfType<GameLevel>();
        if (!levelInfo)
        {
            Debug.LogWarning("Could not find level information for level!");

            // TODO: Actually handle this...
        }

        gameUI.Show(levelInfo.LevelName);

        Players.ForEach(p =>
        {
            if (!p.HasJoined) return;
            SetupPlayerForRound(p, levelInfo);
        });

        // QUESTION: Display countdown indicator UI (gives player chance to examine round)?????
        // ANSWER  : Likely not...

        // Begin the round once the timer has finished
        roundStartTime = Time.time;
        IsRoundInProgress = true;
        Players.ForEach(p =>
        {
            if (!p.HasJoined) return;
            p.EnableInput();
        });

        AudioManager.Instance.PlayEffect(roundStartAudio);
    }

    private IEnumerator FinishRound(RoundOverData roundData)
    {
        // Disable player input until the next round
        Players.ForEach(p =>
        {
            if (!p.HasJoined) return;
            p.DisableInput(true);
        });

        float roundTime = Time.time - roundStartTime;
        IsRoundInProgress = false;

        GameRound finishedRound = new GameRound(RoundNumber, roundData.Winner, roundTime);
        RoundsPlayed.Add(finishedRound);

        // Briefly pause before showing the round over UI
        yield return new WaitForSeconds(1f);

        // Show the round over UI to hide the scene switching
        StartCoroutine(roundOverUI.Show(finishedRound, ScoreBug, ScoreMan));
        gameUI.Hide();

        AudioManager.Instance.PlayEffect(roundEndAudio);

        // Briefly pause after showing the round over UI (since it fades in)
        yield return new WaitForSeconds(0.5f);

        // Disable projectiles, particles, etc
        TemporaryObjectsManager.Instance.TemporaryChildren.gameObject.ClearChildren();
        PoolManager.Instance.ReclaimPool("Projectiles");
        Players.ForEach(p => p.gameObject.SetActive(false));

        AsyncOperation unloadLevel = SceneManager.UnloadSceneAsync(currentLevelIndex);
        while (!unloadLevel.isDone)
        {
            yield return null;
        }

        // Load the next level (or may be the same if "debugLevelIndex" is set)
        currentLevelIndex = GetNextLevelIndex(debugLevelIndex);
        AsyncOperation loadLevel = SceneManager.LoadSceneAsync(currentLevelIndex, LoadSceneMode.Additive);
        while (!loadLevel.isDone)
        {
            yield return null;
        }

        StartCoroutine(roundOverUI.Hide(roundOverCountdown, () =>
        {
            StartRound();
        }));
    }

    private void OnRoundOver(IMessage message)
    {
        RoundOverData roundData = (RoundOverData)message.Data;
        StartCoroutine(FinishRound(roundData));
    }
    #endregion

    #region Level Methods
    /// <summary>
    /// Get the next level index
    /// </summary>
    /// <param name="levelIndex">Level index</param>
    /// <returns>Next level index</returns>
    private int GetNextLevelIndex(int levelIndex = -1)
    {
        // Allow selecting a random level if a negative (or invalid) starting level index is used
        return levelIndex < firstLevelIndex || levelIndex >= SceneManager.sceneCountInBuildSettings - 1
            ? GetRandomLevelIndex()
            : levelIndex;
    }

    /// <summary>
    /// Get a random scene/level index
    /// </summary>
    /// <returns>Rarndom scene index</returns>
    private int GetRandomLevelIndex()
    {
        // Ensure that the same level is not chosen consecutively
        int randomIndex = currentLevelIndex;
        while (randomIndex == currentLevelIndex)
        {
            randomIndex = Random.Range(firstLevelIndex, SceneManager.sceneCountInBuildSettings);
        }

        return randomIndex;
    }
    #endregion
    #endregion
}
