﻿using Sirenix.OdinInspector;
using Unavia.Utilities;
using UnityEngine;

/// <summary>
/// Customizable rules manager
/// </summary>
/// <remarks>
/// TODO: Allow rule randomization per round (with base rules, etc)...
/// </remarks>
public class RulesManager : GameSingleton<RulesManager>
{
	#region Fields
	[BoxGroup("Weapons")]
	[Range(0, 5)]
	public int MaxProjectileBounces = 2;
	[BoxGroup("Weapons")]
	public LayerMask ProjectileCollisionLayers = -1;
	[BoxGroup("Weapons")]
	[Range(1, 5)]
	public int MaxShots = 3;
	[BoxGroup("Weapons")]
	[Range(0f, 5f)]
	public float ShotRechargeTime = 1f;

	[BoxGroup("Debug")]
	public bool ShowPlayerLook = true;
	[BoxGroup("Debug")]
	public bool ShowPlayerMovement = true;
	#endregion
}
