﻿using EZCameraShake;
using Unavia.Utilities;
using UnityEngine;

public class Projectile : ExtendedMonoBehaviour, IPoolObject
{
    #region Fields
    [SerializeField]
    [Range(1f, 50f)]
    private float speed = 15f;

    [Header("Game Objects")]
    [SerializeField]
    private GameObject wallParticles = default;
    [SerializeField]
    private GameObject explosionParticles = default;

    [Header("Audio")]
    [SerializeField]
    private AudioClip hitAudio = default;
    [SerializeField]
    private AudioClip explodeAudio = default;
    #endregion

    public GameObject GameObject { get { return gameObject; } }

    private Rigidbody rb;
    private new Light light;
    private RulesManager rules;
    private int bounces = 0;
    private float lastBounceTime = 0f;


    #region Unity Methods
    private void OnCollisionEnter(Collision collision)
    {
        if (!rules.ProjectileCollisionLayers.ContainsLayer(collision.gameObject.layer)) return;

        // Only the first contact is used in deciding where to bounce (reflect) from
        ContactPoint contactPoint = collision.GetContact(0);
        HandleCollision(collision.collider, contactPoint.point, contactPoint.normal);
    }
    #endregion


    #region Custom Methods
    public void OnCreate()
    {
        rb = GetComponent<Rigidbody>();
        light = GetComponentInChildren<Light>();
        rules = RulesManager.Instance;
    }

    // Pooling method
    public void OnSpawn()
    {
        bounces = 0;

        rb.velocity = Vector3.zero;
        rb.AddForce(transform.forward * speed, ForceMode.VelocityChange);
    }

    public void OnSpawnInit(Color color)
    {
        light.color = color;
    }

    // Pooling method
    public void OnReclaim()
    {
        rb.velocity = Vector3.zero;
        gameObject.SetActive(false);
    }

    private void HandleCollision(Collider other, Vector3 hitPoint, Vector3 hitNormal)
    {
        PlayerController otherPlayer = other.GetComponent<PlayerController>();
        if (otherPlayer)
        {
            otherPlayer.Hit();
            Explode();
            return;
        }

        Projectile otherProjectile = other.GetComponent<Projectile>();
        if (otherProjectile)
        {
            otherProjectile.Explode();
            Explode();
            return;
        }

        // NOTE: Ignore bounces that occur too quickly (ie. same frame, etc)
        if (Time.time <= lastBounceTime + Time.deltaTime) return;

        // Projectiles have a maximum bounce limit
        if (bounces >= rules.MaxProjectileBounces)
        {
            Explode();
            return;
        }

        AudioManager.Instance.PlayEffect(hitAudio, hitPoint, 1f);

        Instantiate(
            wallParticles,
            hitPoint,
            Quaternion.Euler(hitNormal),
            TemporaryObjectsManager.Instance.TemporaryChildren
        );

        bounces += 1;
        lastBounceTime = Time.time;
    }

    /// <summary>
    /// Explode the projectile
    /// </summary>
    public void Explode()
    {
        AudioManager.Instance.PlayEffect(explodeAudio, transform.position, 1f);

        Instantiate(
            explosionParticles,
            transform.position,
            Quaternion.identity,
            TemporaryObjectsManager.Instance.TemporaryChildren
        );

        CameraShaker.Instance.ShakeOnce(2f, 3f, 0.05f, 0.4f);

        OnReclaim();
    }
    #endregion
}
