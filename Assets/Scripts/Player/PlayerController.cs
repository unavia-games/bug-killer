﻿using com.ootii.Messages;
using EZCameraShake;
using Sirenix.OdinInspector;
using Unavia.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public enum PlayerType
{
    BUG,
    MAN
}

public class PlayerController : ExtendedMonoBehaviour
{
    #region Fields
    [Header("Properties")]
	public PlayerType Type = default;
    [SerializeField]
	[Range(1f, 10f)]
	private float speed = 5f;
	[ReadOnly]
	public bool HasJoined = false;
    public float YSpawnOffset = 0.5f;
    [SerializeField]
    private Color projectileColor = Color.white;

    [Header("Game Objects")]
    [SerializeField]
    private GameObject shotIndicatorLayout = default;
    [SerializeField]
    private GameObject shotIndicatorPrefab = default;
    [SerializeField]
    private GameObject deathParticles = default;
    [SerializeField]
    private Transform fireTransform = default;

    [Header("Audio")]
    [SerializeField]
    private AudioClip shootAudio = default;
    #endregion

    private Rigidbody rb;
    public PlayerInput Input { get; private set; }
    private GameManager gameManager;
    private InputActionMap playerActions;
    private RulesManager rules;

    private int shotsRemaining;
    private float timeSinceRecharge = 0f;
    private Image[] shotIndicators;
    private float emptyShotIndicatorAlpha = 0.1f;

    private Vector3 lookDirection = Vector3.zero;
    private Vector3 moveDirection = Vector3.zero;

    // Tolerances prevent strange behaviour (opposites, etc) when sticks are released
    //   and snap back into position (move should be smaller than look).
    // NOTE: Uses square root calculations to avoid magnitude checks later
    private float moveToleranceSqr = Mathf.Pow(0.25f, 2);
    private float lookToleranceSqr = Mathf.Pow(0.25f, 2);


    #region Unity Methods
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        rules = RulesManager.Instance;
        gameManager = GameManager.Instance;
    }

    private void OnEnable()
    {
        SubscribeToInput(Input);
    }

    private void OnDisable()
    {
        UnsubscribeFromInput(Input);
    }

    void Update()
    {
        // Player's projectiles can recharge over time
        if (shotsRemaining < rules.MaxShots)
        {
            timeSinceRecharge += Time.deltaTime;

            if (timeSinceRecharge >= rules.ShotRechargeTime)
            {
                shotsRemaining++;
                UpdateShotIndicators();
                timeSinceRecharge = 0f;
            }
        }
    }

    private void FixedUpdate()
    {
        // Apply values from input callbacks
        Vector3 velocity = moveDirection * speed;
        rb.MovePosition(transform.position + velocity * Time.deltaTime);

        if (lookDirection != Vector3.zero)
        {
            Quaternion rotation = Quaternion.LookRotation(lookDirection, transform.up);
            rb.MoveRotation(rotation);
        }
    }
    #endregion


    #region Custom Methods
    #region Input Handling Methods
    public void SetInput(PlayerInput _input)
    {
        Input = _input;
        HasJoined = true;

        SubscribeToInput(Input);
    }

    public void RemoveInput(PlayerInput _input)
    {
        Input = null;
        HasJoined = false;

        UnsubscribeFromInput(_input);
    }

    private void SubscribeToInput(PlayerInput input)
    {
        if (input == null) return;

        // NOTE: Unsubscribing prevents any double subscription bugs
        UnsubscribeFromInput(input);

        playerActions = input.actions.FindActionMap("Player");

        playerActions["Movement"].performed += OnMovement;
        playerActions["Movement"].canceled += OnMovement;
        playerActions["Look"].performed += OnLook;
        playerActions["Fire"].started += OnFire;
    }

    private void UnsubscribeFromInput(PlayerInput input)
    {
        if (input == null) return;

        playerActions = input.actions.FindActionMap("Player");

        playerActions["Movement"].performed -= OnMovement;
        playerActions["Movement"].canceled -= OnMovement;
        playerActions["Look"].performed -= OnLook;
        playerActions["Fire"].started -= OnFire;
    }

    public void EnableInput()
    {
        Input.currentActionMap.Enable();
    }

    public void DisableInput(bool canLook = false)
    {
        Input.currentActionMap.Disable();

        // Player can occasioanlly look around after input is disabled
        if (canLook)
        {
            playerActions["Look"].Enable();
        }

        // Also disable current player movement (speed)
        moveDirection = Vector3.zero;
        if (rb != null)
        {
            rb.velocity = Vector3.zero;
        }
    }
    #endregion

    /// <summary>
    /// Reset player direction and position within parent shell
    /// </summary>
    public void Reset()
    {
        transform.localPosition = Vector3.zero + transform.up * YSpawnOffset;
        transform.localRotation = Quaternion.identity;

        lookDirection = Vector3.zero;
        moveDirection = Vector3.zero;
        rb.velocity = Vector3.zero;

        // Player should start with one shot
        shotsRemaining = 1;
        timeSinceRecharge = 0f;

        // Create shot indicators (needs Start method for "Rules" reference)
        shotIndicatorLayout.ClearChildren();
        shotIndicators = new Image[RulesManager.Instance.MaxShots];
        for (int i = shotIndicators.Length - 1; i >= 0; i--)
        {
            GameObject imageObject = Instantiate(shotIndicatorPrefab);
            imageObject.transform.SetParent(shotIndicatorLayout.transform, false);
            shotIndicators[i] = imageObject.GetComponent<Image>();
        }

        UpdateShotIndicators();
    }

    public void Hit()
    {
        // Skip any hits received after the round ended
        if (!gameManager.IsRoundInProgress) return;

        // Colliding with own projectile makes the other player win
        PlayerType winner = Type == PlayerType.MAN ? PlayerType.BUG : PlayerType.MAN;

        RoundOverData roundOverData = new RoundOverData(winner);
        MessageDispatcher.SendMessageData(GameEventStrings.ROUND__OVER, roundOverData, -1);

        CameraShaker.Instance.ShakeOnce(5f, 5f, 0.1f, 1.25f);
        Instantiate(
            deathParticles,
            transform.position,
            Quaternion.identity,
            TemporaryObjectsManager.Instance.TemporaryChildren
        );

        gameObject.SetActive(false);
    }

    /// <summary>
    /// Update the shot indicator icons
    /// </summary>
    private void UpdateShotIndicators()
    {
        for (int i = 0; i < shotIndicators.Length; i++)
        {
            Color indicatorColor = shotIndicators[i].color;
            indicatorColor.a = i < shotsRemaining ? 1f : emptyShotIndicatorAlpha;
            shotIndicators[i].GetComponent<Image>().color = indicatorColor;
        }
    }

    public void OnFire(InputAction.CallbackContext ctx)
    {
        if (shotsRemaining <= 0) return;

        GameObject projectileObject = PoolManager.Instance.SpawnFromPool(
            "Projectiles",
            fireTransform.position,
            fireTransform.rotation
        );
        Projectile projectile = projectileObject.GetComponent<Projectile>();
        projectile.OnSpawnInit(projectileColor);

        AudioManager.Instance.PlayEffect(shootAudio, fireTransform.position, 1f);

        shotsRemaining--;
        timeSinceRecharge = 0f;
        UpdateShotIndicators();
    }

    public void OnLook(InputAction.CallbackContext ctx)
    {
        Vector2 input = ctx.ReadValue<Vector2>();

        // Look input under 0.5 may indicate that the user stopped pressing the stick,
        //   which may swing the look direction in the opposite direction if not handled.
        if (input.sqrMagnitude < lookToleranceSqr) return;

        // Normalize the look direction to ensure it remains at a consistent distance away.
        Vector2 normalizedLook = input.normalized;
        // Prevent view from shifting up by zeroing out 'y' transform
        lookDirection = new Vector3(normalizedLook.x, 0f, normalizedLook.y);
    }

    public void OnMovement(InputAction.CallbackContext ctx)
    {
        Vector2 input = ctx.ReadValue<Vector2>();

        // Movement input under 0.25 may indicate that the user stopped pressing the stick,
        //   which may swing the movement direction in the opposite direction if not handled.
        // However, a zeroed out vector indicates stopping (which must be retained).
        if (input != Vector2.zero && input.sqrMagnitude < moveToleranceSqr) return;

        // Prevent player from moving up by zeroing out 'y' transform
        moveDirection = new Vector3(input.x, 0f, input.y);
    }
    #endregion
}
