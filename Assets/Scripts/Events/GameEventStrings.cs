﻿public static class GameEventStrings
{
    public const string PLAYER__JOIN = "PLAYER__JOIN";

    public const string ROUND__OVER = "ROUND__OVER";
}
