﻿public class PlayerJoinData
{
    public PlayerType Player;

    public PlayerJoinData(PlayerType type)
    {
        Player = type;
    }
}

public class RoundOverData
{
    public PlayerType Winner;

    public RoundOverData(PlayerType winner)
    {
        Winner = winner;
    }
}
