﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.ProceduralImage;

public class RoundOverUI : MonoBehaviour
{
    #region Fields
    [SerializeField]
    private GameObject screen = default;
    [SerializeField]
    private TextMeshProUGUI roundNumber = default;

    [SerializeField]
    private TextMeshProUGUI counterBug = default;
    [SerializeField]
    private TextMeshProUGUI counterMan = default;
    [SerializeField]
    private Image logoBug = default;
    [SerializeField]
    private Image logoMan = default;
    [SerializeField]
    private ProceduralImage countdownIndicator = default;
    #endregion

    private CanvasGroup screenCanvasGroup = default;
    private float fadeTime = 0.25f;
    private float loserTextAlpha = 0.250f;
    private float winnerTextAlpha = 1f;
    private Color loserLogoColor = new Color(255, 255, 255, 0.25f);
    private Color winnerLogoColor = new Color(255, 255, 255, 1f);


    #region Unity Methods
    private void Awake()
    {
        screenCanvasGroup = screen.GetComponent<CanvasGroup>();
    }
    #endregion


    #region Custom Methods
    public IEnumerator Show(GameRound round, int scoreBug, int scoreMan)
    {
        screen.SetActive(true);
        screenCanvasGroup.alpha = 0f;
        countdownIndicator.fillAmount = 0f;

        roundNumber.text = $"Round {round.Number}";
        counterBug.text = scoreBug.ToString();
        counterBug.alpha = round.Winner == PlayerType.BUG ? winnerTextAlpha : loserTextAlpha;
        logoBug.color = round.Winner == PlayerType.BUG ? winnerLogoColor : loserLogoColor;
        counterMan.text = scoreMan.ToString();
        counterMan.alpha = round.Winner == PlayerType.MAN ? winnerTextAlpha : loserTextAlpha;
        logoMan.color = round.Winner == PlayerType.MAN ? winnerLogoColor : loserLogoColor;

        // Fade screen to new level
        while (screenCanvasGroup.alpha < 1f)
        {
            screenCanvasGroup.alpha += (1 / fadeTime * Time.deltaTime);
            yield return null;
        }
    }

    /// <summary>
    /// Fade the screen out
    /// </summary>
    /// <param name="time">Time to hide screen</param>
    /// <param name="callback">Callback</param>
    /// <returns>Coroutine</returns>
    public IEnumerator Hide(float time, Action callback)
    {
        while (countdownIndicator.fillAmount < 1f)
        {
            countdownIndicator.fillAmount += (1 / time * Time.deltaTime);
            yield return null;
        }

        // NOTE: Callback is invoked before fading so that objects are in place
        callback?.Invoke();

        // Fade screen to new level
        while (screenCanvasGroup.alpha > 0f)
        {
            screenCanvasGroup.alpha -= (1 / fadeTime * Time.deltaTime);
            yield return null;
        }

        screen.SetActive(false);
    }
    #endregion
}
