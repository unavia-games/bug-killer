﻿using com.ootii.Messages;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI.ProceduralImage;

public class MainMenuUI : MonoBehaviour
{
    #region Fields
    [SerializeField]
    private GameObject screen = default;

    [SerializeField]
    private CanvasGroup logoSideBug = default;
    [SerializeField]
    private CanvasGroup logoSideMan = default;

    [SerializeField]
    private CanvasGroup controllerBug = default;
    [SerializeField]
    private CanvasGroup controllerMan = default;
    [SerializeField]
    private ProceduralImage joinIndicator = default;
    #endregion

    private CanvasGroup screenCanvasGroup = default;
    private float logoUnselectedAlpha = 0.25f;
    private float logoSelectedAlpha = 1f;
    private float controllerUnselectedAlpha = 0.15f;
    private float controllerSelectedAlpha = 1f;


    #region Unity Methods
    private void Awake()
    {
        screenCanvasGroup = screen.GetComponent<CanvasGroup>();

        MessageDispatcher.AddListener(GameEventStrings.PLAYER__JOIN, OnPlayerJoin);
    }

    private void OnDestroy()
    {
        MessageDispatcher.AddListener(GameEventStrings.PLAYER__JOIN, OnPlayerJoin);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Show the menu UI
    /// </summary>
    public void Show()
    {
        logoSideBug.alpha = logoUnselectedAlpha;
        logoSideMan.alpha = logoUnselectedAlpha;

        controllerBug.alpha = controllerUnselectedAlpha;
        controllerMan.alpha = controllerUnselectedAlpha;

        joinIndicator.fillAmount = 0f;

        screen.SetActive(true);
    }

    /// <summary>
    /// Update the indicator showing that the game will begin soon
    /// </summary>
    /// <param name="time">Delay until game starts</param>
    /// <param name="callback">Finish callback</param>
    /// <returns>Coroutine</returns>
    public IEnumerator FadeToGame(float time, Action callback)
    {
        // "Countdown" game indicator
        while (joinIndicator.fillAmount < 1f)
        {
            joinIndicator.fillAmount += (1 / time * Time.deltaTime);
            yield return null;
        }

        // NOTE: Callback is invoked before fading so that objects are in place
        callback?.Invoke();

        // Fade screen to first level
        float fadeTime = 0.25f;
        while (screenCanvasGroup.alpha > 0f)
        {
            screenCanvasGroup.alpha -= (1 / fadeTime * Time.deltaTime);
            yield return null;
        }

        screen.SetActive(false);
    }

    private void OnPlayerJoin(IMessage message)
    {
        PlayerJoinData data = (PlayerJoinData)message.Data;

        if (data.Player == PlayerType.BUG)
        {
            controllerBug.alpha = controllerSelectedAlpha;
            logoSideBug.alpha = logoSelectedAlpha;
        }
        else
        {
            controllerMan.alpha = controllerSelectedAlpha;
            logoSideMan.alpha = logoSelectedAlpha;
        }
    }
    #endregion
}
