﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    #region Fields
    [SerializeField]
    private GameObject screen = default;
    [SerializeField]
    private TextMeshProUGUI levelName = default;
    #endregion


    #region Unity Methods
    #endregion


    #region Custom Methods
    public void Show(string levelName)
    {
        screen.SetActive(true);

        this.levelName.text = levelName;
    }

    /// <summary>
    /// Fade the screen out
    /// </summary>
    public void Hide()
    {
        screen.SetActive(false);
    }
    #endregion
}
