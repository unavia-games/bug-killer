﻿namespace Unavia.Utilities
{
    public static class ArrayExtensions
    {
        #region Custom Methods
        /// <summary>
        /// Shuffle an array using Fisher-Yates algorithm
        /// </summary>
        /// <typeparam name="T">Array type</typeparam>
        /// <param name="array">Unshuffled array</param>
        /// <param name="seed">Shuffling randomizer seed</param>
        /// <returns>Shuffled array</returns>
        public static T[] Shuffle<T>(this T[] array, int seed)
        {
            System.Random generator = new System.Random(seed);

            T[] shuffledArray = (T[])array.Clone();

            for (int i = 0; i < array.Length - 1; i++)
            {
                int randomIndex = generator.Next(i, array.Length);

                T temp = shuffledArray[randomIndex];
                shuffledArray[randomIndex] = shuffledArray[i];
                shuffledArray[i] = temp;
            }

            return shuffledArray;
        }
        #endregion
    }
}
