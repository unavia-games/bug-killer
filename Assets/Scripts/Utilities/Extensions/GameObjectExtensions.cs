﻿using UnityEngine;

namespace Unavia.Utilities
{
    public static class GameObjectExtensions
    {
        /// <summary>
        /// Destroy the children of a game object
        /// </summary>
        /// <param name="parent">Parent game object.</param>
        public static void ClearChildren(this GameObject parent)
        {
            int childrenCount = parent.transform.childCount;

            for (int i = childrenCount - 1; i >= 0; i--)
            {
                if (Application.isPlaying)
                {
                    Object.Destroy(parent.transform.GetChild(i).gameObject);
                }
                else
                {
                    Object.DestroyImmediate(parent.transform.GetChild(i).gameObject);
                }
            }
        }

        /// <summary>
        /// Create a nested GameObject in the hierarchy
        /// </summary>
        /// <param name="parent">Parent transform</param>
        /// <param name="childName">New GameObject name</param>
        /// <returns>Nested GameObject</returns>
        public static GameObject CreateGameObject(this GameObject parent, string childName)
        {
            GameObject child = new GameObject(childName);
            child.transform.SetParent(parent.transform);
            child.transform.localPosition = Vector3.zero;

            return child;
        }

        /// <summary>
        /// Create a nested primitive GameObject in the hierarchy
        /// </summary>
        /// <param name="parent">Parent transform</param>
        /// <param name="childName">New GameObject name</param>
        /// <param name="type">New GameObject primitive type</param>
        /// <returns>Nested primitive GameObject</returns>
        public static GameObject CreateGameObject(this GameObject parent, string childName, PrimitiveType type)
        {
            GameObject child = GameObject.CreatePrimitive(type);
            child.name = childName;
            child.transform.SetParent(parent.transform);
            child.transform.localPosition = Vector3.zero;

            return child;
        }
    }
}
