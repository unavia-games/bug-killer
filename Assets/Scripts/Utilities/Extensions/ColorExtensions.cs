﻿using UnityEngine;

namespace Unavia.Utilities
{
    public static class ColorExtensions
    {
        /// <summary>
        /// Make a color lighter or darker
        /// </summary>
        /// <param name="color">Color to correct.</param>
        /// <param name="correctionFactor">The brightness correction factor. Must be between -1 and 1. 
        /// Negative values produce darker colors.</param>
        /// <returns>
        /// Corrected <see cref="Color"/> structure.
        /// </returns>
        /// <remarks>Taken from: https://gist.github.com/zihotki/09fc41d52981fb6f93a81ebf20b35cd5</remarks>
        public static Color ChangeColorBrightness(this Color color, float correctionFactor)
        {
            float red = color.r;
            float green = color.g;
            float blue = color.b;

            if (correctionFactor < 0)
            {
                correctionFactor = 1 + correctionFactor;
                red *= correctionFactor;
                green *= correctionFactor;
                blue *= correctionFactor;
            }
            else
            {
                red = (255 - red) * correctionFactor + red;
                green = (255 - green) * correctionFactor + green;
                blue = (255 - blue) * correctionFactor + blue;
            }

            return new Color(red, green, blue, color.a);
        }
    }
}
