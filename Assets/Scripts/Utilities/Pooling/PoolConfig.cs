﻿using Sirenix.OdinInspector;
using System;
using UnityEngine;

namespace Unavia.Utilities
{
    [Serializable]
    public class PoolConfig
    {
        #region Fields
        public string Name;
        [InfoBox("Requires a reference to the IPoolObject component itself, rather than its GameObject!", infoMessageType: InfoMessageType.Warning)]
        [SerializeField]
        [Required]
        [RequireInterface(typeof(IPoolObject))]
        private UnityEngine.Object poolObject = default;
        [Range(1, 100)]
        public int Size = 10;
        public Transform SpawnParent = default;

        // NOTE: This is not serialized
        public IPoolObject PoolObject => poolObject as IPoolObject;
        #endregion
    }
}
