﻿using System;
using System.Collections;
using UnityEngine;

namespace Unavia.Utilities
{
    /// <summary>
    /// Extended MonoBehaviour utilities
    /// </summary>
    public class ExtendedMonoBehaviour : MonoBehaviour
    {
        #region Custom Methods
        /// <summary>
        /// Enable waiting for a timeout before performing a delegate function
        /// </summary>
        /// <param name="seconds">Timeout duration</param>
        /// <param name="action">Delegate callback</param>
        public Coroutine Wait(float seconds, Action action)
        {
            return StartCoroutine(SetTimeout(seconds, action));
        }

        /// <summary>
        /// Perform delegate function after timeout
        /// </summary>
        /// <param name="time">Timeout duration</param>
        /// <param name="callback">Delegate callback</param>
        /// <returns></returns>
        private IEnumerator SetTimeout(float time, Action callback)
        {
            yield return new WaitForSeconds(time);

            callback();
        }

        /// <summary>
        /// Clear a transform's children
        /// </summary>
        /// <param name="objectTransform">Parent game object transform</param>
        protected void ClearChildren(Transform objectTransform)
        {
            int childrenCount = objectTransform.childCount;

            for (int i = childrenCount - 1; i >= 0; i--)
            {
                // Assets are destroyed differently while playing vs in the editor
                if (Application.isPlaying)
                {
                    Destroy(objectTransform.GetChild(i).gameObject);
                }
                else
                {
                    DestroyImmediate(objectTransform.GetChild(i).gameObject);
                }
            }
        }

        /// <summary>
        /// Remove component from an object
        /// </summary>
        /// <param name="components">Component to remove</param>
        protected void RemoveComponent(Component component)
        {
            if (Application.isPlaying)
            {
                Destroy(component);
            }
            else
            {
                DestroyImmediate(component);
            }
        }

        /// <summary>
        /// Remove components from an object
        /// </summary>
        /// <param name="components">Components to remove</param>
        protected void RemoveComponents(Component[] components)
        {
            for (int i = components.Length - 1; i >= 0; i--)
            {
                RemoveComponent(components[i]);
            }
        }
        #endregion
    }
}
