﻿using UnityEngine;

public class DisplacementShaderTest : MonoBehaviour
{
    #region Fields
    [Range(0f, 2f)]
    public float Speed = 0.5f;
    public bool UseTimer = false;
    #endregion

    private Material material;
    private float displacementAmount = 0f;
    private float timeSinceLastDisplacement = 0f;


    #region Unity Methods
    void Awake()
    {
        material = GetComponent<Renderer>().material;
    }

    void Update()
    {
        if (UseTimer)
        {
            if (timeSinceLastDisplacement > 5f)
            {
                displacementAmount += 0.5f;
                timeSinceLastDisplacement = 0f;
            }

            timeSinceLastDisplacement += Time.deltaTime;

            displacementAmount = Mathf.Lerp(displacementAmount, 0, Time.deltaTime);
        }
        else
        {
            displacementAmount = 0.25f;
        }

        material.SetFloat("_Amount", displacementAmount);
        material.SetFloat("_Speed", Speed);
    }
    #endregion


    #region Custom Methods
    #endregion
}
